#!/usr/bin/env python

from cx_Freeze import Executable, setup


executables = [Executable('main.py', targetName='qtcalc', icon='icon.ico')]

excludes = ['email', 'ctypes', 'http', 'distutils',
        'html', 'logging', 'urllib', 'unittest', 'xml', 'xmlrpc']

includes = ['PyQt5']

options = {
    'build_exe':
    {
        'excludes': excludes,
        'includes': includes,
    }
}

setup(name='Qt Calculator', version='0.2',
        description='Gui Calculator in PyQt5',
        executables=executables, options=options)
