# QtCalculator

### Cross-platform Gui Qt calculator with keyboard input and factorial count


#### Instalation:
###### 1)Download python3 + version
###### 2)Run command ```pip3 install requirements.txt```
###### 3)Run setup ```./setup.py build```

#### If you want build package

##### Windows:

```./setup.py bdist_wininst```

#### Linux (tar.gz):
```./setup.py bdist```

#### Linux (rpm):
```./setup.py bdist_rpm```