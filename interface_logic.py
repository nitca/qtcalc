from PyQt5 import QtWidgets
from get_keys import text_from_keyboard
from calc_interface import Ui_MainWindow


def check_for_two_num(problem):
    """checks before solve a problem"""
    symbols = ['+', '-', '*', '/', '%']
    for symbol in symbols:
        if problem.find(symbol) != -1:
            if problem[-1].isdigit():
                return True
    return False


def fact(problem, interface):
    """Factorial count"""
    digits = ""
    first_part = ""
    second_part = ""
    answer = 1
    answer_return = ""

    mark_pos = problem.find('!')
    i = mark_pos-1

    while problem[i].isdigit() and i >= 0:
        digits += problem[i]
        i -= 1

    if i > 0:
        first_part = problem[:i+1]
    if mark_pos+1 < len(problem):
        second_part = problem[mark_pos+1:]

    try:
        digits = int(digits)
        for digit in range(1, digits + 1):
            answer *= digit
    except ValueError:
        message = QtWidgets.QMessageBox(interface)
        message.warning(interface, 'Syntax error',
            'Syntax Error. Check your input', QtWidgets.QMessageBox.Ok)
        interface.display_text = '0'

    answer = str(answer)
    answer_return += first_part
    answer_return += answer
    answer_return += second_part
    return answer_return


class CalcInterface(QtWidgets.QMainWindow):
    def __init__(self):
        """All buttons, buttons logic and problem's logic"""
        super(CalcInterface, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.display_text = ""

        self.ui.btn0.clicked.connect(self.btn0)
        self.ui.btn1.clicked.connect(self.btn1)
        self.ui.btn2.clicked.connect(self.btn2)
        self.ui.btn3.clicked.connect(self.btn3)
        self.ui.btn4.clicked.connect(self.btn4)
        self.ui.btn5.clicked.connect(self.btn5)
        self.ui.btn6.clicked.connect(self.btn6)
        self.ui.btn7.clicked.connect(self.btn7)
        self.ui.btn8.clicked.connect(self.btn8)
        self.ui.btn9.clicked.connect(self.btn9)

        self.ui.btnPlus.clicked.connect(self.btnPlus)
        self.ui.btnDis.clicked.connect(self.btnDis)
        self.ui.btnMul.clicked.connect(self.btnMul)
        self.ui.btnDiv.clicked.connect(self.btnDiv)
        self.ui.btnPers.clicked.connect(self.btnPers)
        self.ui.btnDot.clicked.connect(self.btnDot)
        self.ui.btnFact.clicked.connect(self.btnFact)

        self.ui.btnEsc.clicked.connect(self.btnEsc)
        self.ui.btnDel.clicked.connect(self.btnDel)
        self.ui.btnEqual.clicked.connect(self.btnEqual)

    def btnEqual(self):
        if self.display_text.find('!') != -1:
            answer = fact(self.display_text, self)
            self.display_text = answer
            self.ui.display.setText(self.display_text)

        if check_for_two_num(self.display_text):
            tmp_problem = self.display_text
            try:
                self.display_text = str(eval(tmp_problem))
            except SyntaxError:
                message = QtWidgets.QMessageBox(self)
                message.warning(self, 'Syntax error',\
                  'Syntax Error. Check your input', QtWidgets.QMessageBox.Ok)
                self.display_text = ''
            except ZeroDivisionError:
                message = QtWidgets.QMessageBox(self)
                message.warning(self, 'Zero Devision',\
                  'Do not try divide to zero', QtWidgets.QMessageBox.Ok)
                self.display_text = ''

            self.ui.display.setText(self.display_text)

    def btnDel(self):
        self.display_text = self.display_text[:-1]
        self.ui.display.setText(self.display_text)

    def btnEsc(self):
        self.display_text = ''
        self.ui.display.clear()

    def btnFact(self):
        if len(self.display_text) > 0:
            self.display_text += '!'
            self.ui.display.setText(self.display_text)

    def btnDiv(self):
        if len(self.display_text) > 0:
            self.display_text += '/'
            self.ui.display.setText(self.display_text)

    def btnPers(self):
        if len(self.display_text) > 0:
            self.display_text += '%'
            self.ui.display.setText(self.display_text)

    def btnDot(self):
        if len(self.display_text) > 0:
            self.display_text += '.'
            self.ui.display.setText(self.display_text)

    def btnMul(self):
        if len(self.display_text) > 0:
            self.display_text += '*'
            self.ui.display.setText(self.display_text)

    def btnDis(self):
        if len(self.display_text) > 0:
            self.display_text += '-'
            self.ui.display.setText(self.display_text)

    def btnPlus(self):
        if len(self.display_text) > 0:
            self.display_text += '+'
            self.ui.display.setText(self.display_text)

    def btn0(self):
        self.display_text += '0'
        self.ui.display.setText(self.display_text)

    def btn1(self):
        self.display_text += '1'
        self.ui.display.setText(self.display_text)

    def btn2(self):
        self.display_text += '2'
        self.ui.display.setText(self.display_text)

    def btn3(self):
        self.display_text += '3'
        self.ui.display.setText(self.display_text)

    def btn4(self):
        self.display_text += '4'
        self.ui.display.setText(self.display_text)

    def btn5(self):
        self.display_text += '5'
        self.ui.display.setText(self.display_text)

    def btn6(self):
        self.display_text += '6'
        self.ui.display.setText(self.display_text)

    def btn7(self):
        self.display_text += '7'
        self.ui.display.setText(self.display_text)

    def btn8(self):
        self.display_text += '8'
        self.ui.display.setText(self.display_text)

    def btn9(self):
        self.display_text += '9'
        self.ui.display.setText(self.display_text)

    def keyPressEvent(self, event):
        if event:
            text_from_keyboard(self, event)
        else:
            super(CalcInterface, self).keyPressEvent(event)
