#!/usr/bin/env python

import sys
import PyQt5
from interface_logic import CalcInterface


if __name__ == '__main__':
    app = PyQt5.QtWidgets.QApplication([])

    win = CalcInterface()
    win.show()

    sys.exit(app.exec_())
